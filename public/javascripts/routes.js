(function() {
	'use strict';
	
	angular
		.module('fucsia.app')
		.config(routes);
	
	routes.$inject = ['$stateProvider'];
	
	function routes($stateProvider) {
		$stateProvider
			.state('productos', {
				url: '/productos',
				templateUrl: 'views/partials/productos/index.jade',
				controller: 'ProductosController',
				controllerAs: 'vm'
			})
			.state('productos.listado', {
				url: '/listado',
				templateUrl: 'views/partials/productos/listado.jade'
			})
			.state('productos.nuevo', {
				url: '/nuevo',
				templateUrl: 'views/partials/productos/nuevo.jade'
			});
	}	
	
})();