(function() {
	angular
		.module('fucsia.app')
		.controller('ProductosController', ProductosController);
		
	ProductosController.$inject = ['API', 'DTOptionsBuilder', 'DTColumnBuilder', '$state'];
	
	function ProductosController(API, DTOptionsBuilder, DTColumnBuilder, $state) {
		var vm = this;
		
		vm.productos = [];
		
		vm.guardar = guardar;
		
		// initialization
		(function() {
			vm.dtOptions = {
				ajax: {
					url: 'api/productos',
					dataSrc: ''
				},
				order: [[3, 'asc']],
				stateSave: true,
				language: {
					url: 'misc/datatables-i18n-spanish.json'
				},
				aoColumns: [
					{ data: '__v', name: 'version', visible: false, searchable: false },
					{ data: '_id', name: 'id', visible: false, searchable: false },
					{ data: 'codigo', title: 'Codigo', name: 'codigo' },
					{ data: 'nombre', title: 'Nombre', name: 'nombre' },
					{ data: 'categorias[, ]', title: 'Categorias', name: 'categorias' },
					{ data: 'created_at', title: 'Fecha Creacion', name: 'fecha_creacion' },
					{ data: 'updated_at', title: 'Fecha modificacion', name:' fecha_modificacion' },
					{ data: null, 'class': 'action', defaultContent: '', orderable: false, render: renderActionButtons }
				]
			};			
			
			function renderActionButtons(data) {
				return '<button class="btn btn-success btn-xs"><i class="fa fa-pencil"></i></button> ' +
					'<button class="btn btn-danger btn-xs"><i class="fa fa-times"></i></button>';
			}
		})();
		
		function guardar() {
			API.Productos.save(vm.nuevo, function(prod) {
				if (prod) {
					$state.go('productos.listado');
				}
			});
		}
	}
})();