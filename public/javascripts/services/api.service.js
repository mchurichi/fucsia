(function() {
	angular
		.module('fucsia.app')
		.factory('API', APIService);
	
	APIService.$inject = ['$resource'];
	
	function APIService($resource) {
		return {
			Productos: $resource('/api/productos/:id', {id:'@id'})
		}
	}
})();