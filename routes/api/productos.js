var express = require('express');
var mongoose = require('mongoose');
var router = express.Router();

var Producto = require('../../models/Producto');

router
  .post('/', function (req, res, next) {
    Producto.create(req.body,
      function (err, prod) {
        if (!err) {
          res.send(prod);
        } else {
          res.send(400, 'Error al guardar');
        }
      });
  })
  .get('/', function(req, res, next) {
    Producto.find(req.body, function(err, prods) {
      if (!err) {
        res.send(prods);
      } else {
        res.send(400, 'Error al obtener listado de productos');
      }
    });
  })
  .get('/:id', function(req, res, next) {
    Producto.findById(req.params.id, function(err, prod) {
      if (!err) {
        res.send(prod || {});
      } else {
        res.send(400, 'Error al obtener producto');
      }
    });
  });

module.exports = router;