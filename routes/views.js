var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('*.jade', function(req, res, next) {
  res.render(req.params[0].substr(1));
});

module.exports = router;
