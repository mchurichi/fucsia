var mongoose = require('mongoose');

var ProductoSchema = new mongoose.Schema({
  codigo: String,
  nombre: String,
  categorias: [String],
  created_at: { type: Date },
  updated_at: { type: Date }
});

ProductoSchema.pre('save', function(next){
  var now = new Date();
  this.updated_at = now;
  if ( !this.created_at ) {
    this.created_at = now;
  }
  next();
});

module.exports = mongoose.model('Productos', ProductoSchema);